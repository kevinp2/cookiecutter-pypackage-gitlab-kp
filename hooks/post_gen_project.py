#!/usr/bin/env python
import os
import shutil

PROJECT_DIRECTORY = os.path.realpath(os.path.curdir)


def remove_file(filepath):
    os.remove(os.path.join(PROJECT_DIRECTORY, filepath))

def rename_file(oldfilepath, newfilepath):
    os.rename(os.path.join(PROJECT_DIRECTORY, oldfilepath),
    os.path.join(PROJECT_DIRECTORY, newfilepath)
    )

if __name__ == "__main__":

    # Process author files
    if "{{ cookiecutter.create_author_file }}" != "y":
        remove_file("AUTHORS.rst")
        remove_file("docs/authors.rst")

    # Process class files
    if "{{ cookiecutter.create_class }}" != "y":
        remove_file("CLASSFILE.py")
    else:
        # Move the class file into the package
        rename_file ( "CLASSFILE.py", "{{cookiecutter.project_slug}}/{{cookiecutter.class_name}}.py")

    # Remove gitlab files if not needed
    if "{{ cookiecutter.gitlab_support }}" != "y":
        remove_file(".gitignore")
        remove_file(".gitlab-ci.yml")
        shutil.rmtree(".gitlab")
