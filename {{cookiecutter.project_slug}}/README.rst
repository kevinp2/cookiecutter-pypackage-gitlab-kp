{% set is_open_source = cookiecutter.open_source_license != 'Not open source' -%}
{% for _ in cookiecutter.project_name %}={% endfor %}
{{ cookiecutter.project_name }}
{% for _ in cookiecutter.project_name %}={% endfor %}

{#  Commented out for now

.. image:: {{ cookiecutter.project_url}}/badges/master/pipeline.svg
    :target: {{ cookiecutter.project_url}}/pipelines/
    :alt: Build Status

.. image:: https://img.shields.io/pypi/v/{{ cookiecutter.project_slug }}.svg
    :target: https://pypi.org/pypi/{{ cookiecutter.project_slug }}
    :alt: PyPI

.. image:: {{ cookiecutter.project_url}}/badges/master/coverage.svg
    :target: {{ cookiecutter.project_url}}/pipelines/
    :alt: Coverage


.. image:: https://readthedocs.org/projects/{{ cookiecutter.project_slug | replace("_", "-") }}/badge/?version=latest
        :target: https://{{ cookiecutter.project_slug | replace("_", "-") }}.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status


.. image:: https://img.shields.io/badge/License-{{ cookiecutter.open_source_license | replace("-", "--")}}-blue.svg
    :target: {{ cookiecutter.project_url}}/-/blob/master/LICENSE


#}


{{ cookiecutter.project_short_description }}

{% if is_open_source %}
* Free software: {{ cookiecutter.open_source_license }}
* Documentation: https://{{ cookiecutter.project_slug | replace("_", "-") }}.readthedocs.io.
{% endif %}

{% if not is_open_source %}
* Proprietary: {{ cookiecutter.open_source_license }}
* Documentation: https://{{ cookiecutter.project_slug | replace("_", "-") }}.readthedocs.com.
{% endif %}

Features
--------

* TODO

Development Next Steps
--------

    # Create virtualenv environment (recommended)
    pushd {{cookiecutter.project_slug}}
    python3 -m venv venv
    source venv/bin/activate

    # Install dependencies:

    pip install -r requirements_prod.txt
    pip install -r requirements_dev.txt


{%- if cookiecutter.use_pytest %}

    # Run basic test using pytest:

    chmod 755 tests/test_{{cookiecutter.project_slug}}.py 
    pytest

{%- endif %}

{%- if cookiecutter.main_function == 'y' %}

    # Run main function from the command line 

    chmod 755 {{cookiecutter.project_slug}}/{{cookiecutter.project_slug}}.py 
    {{cookiecutter.project_slug}}/{{cookiecutter.project_slug}}.py --help
    {{cookiecutter.project_slug}}/{{cookiecutter.project_slug}}.py --version
    {{cookiecutter.project_slug}}/{{cookiecutter.project_slug}}.py --logging False
    {{cookiecutter.project_slug}}/{{cookiecutter.project_slug}}.py --logfile ./mylog.log

{%- endif %}

* To exit the development enviroment 

    deactivate
    popd

Credits
-------

This package was created with Cookiecutter and the cookiecutter-pypackage-gitlab-kp project template.

.. Cookiecutter: https://github.com/audreyr/cookiecutter
.. cookiecutter-pypackage-gitlab-kp: https://gitlab.com/kevinp2/cookiecutter-pypackage-gitlab-kp
