#!/usr/bin/env python
"""
Class {{cookiecutter.class_name}}
"""

class {{cookiecutter.class_name}}:
    """
    Documentation for class {{cookiecutter.class_name}}
    """

    # Class attributes shared by all instances
    count = 0   # Count of instances

    def __init__(self, AttributeValue = None):
        """
        Construct a new object of class {{cookiecutter.class_name}}

        Arguments:
        AttributeValue: Optional
        """
        {{cookiecutter.class_name}}.count += 1   # Increase object count

        if AttributeValue == None:
            self.attribute = 0  # Default value
        else:
            self.attribute = AttributeValue

    def __str__ ( self ):
        """
        Prints a string representation of this object
        """

        return ( f"I am an object of class {{cookiecutter.class_name}}" )

    def GetAttributeValue ( self ):
        """
        Example object method.  Returns the value of self.attribute
        """

        return ( self.attribute )
